package com.example.a21765940m.myapplication;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import java.util.List;
import android.databinding.DataBindingUtil;
import com.example.a21765940m.myapplication.databinding.LvPersonajesRowBinding;


public class PersonajesAdapter extends ArrayAdapter<Personaje> {

    public PersonajesAdapter(Context context, int resource, List<Personaje> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Obtenim l'objecte en la possició corresponent
        Personaje pj = getItem(position);
        Log.w("Personaje", pj.getName());

        LvPersonajesRowBinding  binding = null;
        // Mirem a veure si la View s'està reusant, si no es així "inflem" la View
        // https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView#row-view-recycling
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.lv_personajes_row, parent, false);
        } else {
            binding = DataBindingUtil.getBinding(convertView);

        }
        // Fiquem les dades dels objectes (provinents del JSON) en el layout
        binding.lvPersonaje.setText(pj.getName());

        Glide.with(getContext()).load(
                "" + pj.getImageurl()
        ).into(binding.lvImage);

        // Retornem la View replena per a mostrarla
        return binding.getRoot();
    }

}


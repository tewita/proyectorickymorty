package com.example.a21765940m.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.Person;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.app.ProgressDialog;

import android.support.annotation.Nullable;
import android.util.Log;
import android.os.AsyncTask;
import android.view.Menu;
import android.view.MenuInflater;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import com.example.a21765940m.myapplication.databinding.FragmentMainBinding;




/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    private ArrayList<Personaje> items;
    private PersonajesAdapter adapter;
    private SharedPreferences preferences;
    private PersonajesViewModel model;
    private SharedViewModel sharedModel;
    private ProgressDialog dialog;
    private FragmentMainBinding binding;


    public MainActivityFragment() {

    }

    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return onOptionsItemSelected(item);
    }

    private void refresh() {

        model.reload();

    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Personaje>> {
        @Override
        protected ArrayList<Personaje> doInBackground(Void... voids) {
            PersonajesAPI api = new PersonajesAPI();

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());


            String name = preferences.getString("name", "");
            String specie = preferences.getString("specie", "");
            String gender = preferences.getString("gender", "");
            String episode= preferences.getString("episode", "");
            String status= preferences.getString("status", "");

            ArrayList<Personaje> result = api.getPersonajes(name, specie, gender, episode, status);

            if(gender.equals("todos")){
                result=api.getPersonajes(name,specie,"",episode,status);
            }
            else if(status.equals("todos")){
                result=api.getPersonajes(name,specie,gender,episode,"");
            }
            else if(gender.equals("todos") && status.equals("todos")){
                result=api.getPersonajes(name,specie,"",episode,"");
            }

            Log.d("DEBUG", result.toString());

            return result;
        }

        protected void onPostExecute(ArrayList<Personaje> personajes) {

            adapter.clear();
            /**En este bucle le estamos diciendo que meter en el TextView de lv_personaje*/
            for (Personaje person : personajes) {
                adapter.add(person);


            }

        }


    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }
    boolean esTablet(){
        return getResources().getBoolean(R.bool.tablet);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMainBinding.inflate(inflater);
        View view = binding.getRoot();

        String[] data = {

        };


        items = new ArrayList<>();

        adapter = new PersonajesAdapter(
                getContext(),
                R.layout.lv_personajes_row,
                items
        );
        sharedModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        binding.lvPersonajes.setAdapter(adapter);

        binding.lvPersonajes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Personaje pj = (Personaje) adapterView.getItemAtPosition(i);

                if (!esTablet()) {
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("personaje", pj);
                    startActivity(intent);
                }
                else {
                    sharedModel.select(pj);
                }

        }
        });
        model = ViewModelProviders.of(this).get(PersonajesViewModel.class);
        model.getPersonajes().observe(this, new Observer<List<Personaje>>() {
            @Override
            public void onChanged(@Nullable List<Personaje> personaje) {
                adapter.clear();
                adapter.addAll(personaje);
            }
        });

        model = ViewModelProviders.of(this).get(PersonajesViewModel.class);
        model.getPersonajes().observe(this, new Observer<List<Personaje>>() {
            @Override
            public void onChanged(@Nullable List<Personaje> personaje) {
                adapter.clear();
                adapter.addAll(personaje);
            }
        });
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Loading...");

        model.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mostrat) {
                if(mostrat)
                    dialog.show();
                else
                    dialog.dismiss();
            }
        });



        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }


    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_personajes_fragment, menu);
    }
}

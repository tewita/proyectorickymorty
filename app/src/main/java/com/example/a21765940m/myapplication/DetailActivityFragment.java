package com.example.a21765940m.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import com.example.a21765940m.myapplication.databinding.FragmentDetailBinding;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {
    private View view;
    private ImageView lvImageUrl;
    private TextView lvName;
    private TextView lvSpecies;
    private TextView lvStatus;
    private TextView lvGender;
    private TextView lvLocation;
    private FragmentDetailBinding binding;

    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         binding = FragmentDetailBinding.inflate(inflater);
        view = binding.getRoot();


        lvImageUrl = view.findViewById(R.id.lvImage);
        lvName = view.findViewById(R.id.lvPersonaje);
        lvSpecies = view.findViewById(R.id.lvSpecies);
        lvStatus = view.findViewById(R.id.lvStatus);
        lvGender = view.findViewById(R.id.lvGender);
        lvLocation = view.findViewById(R.id.lvLocation);

        Intent i = getActivity().getIntent();

        if (i != null) {
            Personaje pj = (Personaje) i.getSerializableExtra("personaje");

            if (pj != null) {
                updateUi(pj);
            }
        }

        return view;
    }

    private void updateUi(Personaje pj) {
        Log.d("Personaje", pj.toString());

        binding.lvPersonaje.setText(pj.getName());
        binding.lvSpecies.setText(pj.getSpecies());
        binding.lvStatus.setText(pj.getStatus());
        binding.lvGender.setText(pj.getGender());
        binding.lvLocation.setText(pj.getLocation());

        Glide.with(getContext()).load(
                "" + pj.getImageurl()
        ).into(binding.lvImage);


    }

}


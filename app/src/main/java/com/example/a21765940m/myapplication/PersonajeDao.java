package com.example.a21765940m.myapplication;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PersonajeDao {
    @Query("select * from personaje")
    LiveData<List<Personaje>> getPersonajes();

    @Insert
    void addPersonajes(List<Personaje> personaje);

    @Delete
    void deletePersonajes(Personaje personaje);

    @Query("DELETE FROM personaje")
    void deletePersonajes();
}


package com.example.a21765940m.myapplication;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Created by carlesgm on 29/8/17.
 */

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Personaje> selected = new MutableLiveData<Personaje>();

    public void select(Personaje pj) {
        selected.setValue(pj);
    }

    public LiveData<Personaje> getSelected() {
        return selected;
    }
}


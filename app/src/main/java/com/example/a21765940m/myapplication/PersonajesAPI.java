package com.example.a21765940m.myapplication;

import android.app.Person;
import android.net.Uri;

import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class PersonajesAPI {
    private final String BASE_URL =  "https://rickandmortyapi.com/api/character";
    private final int pages = 5;
    ArrayList<Personaje> getPersonajes(String name,String specie,String gender,String episode,String status) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("name",name)
                .appendQueryParameter("species",specie)
                .appendQueryParameter("gender",gender)
                .appendQueryParameter("status",status)

                /**AppendPath '/'*/
                /**AppendQuery '?'*/
                .build();
        String url = builtUri.toString();
        return doCall(name, specie, gender, episode, status);

    }
    private ArrayList<Personaje> doCall(String name,String specie,String gender,String episode,String status) {
        ArrayList<Personaje> personaje= new ArrayList<>();
        for (int i = 0; i < pages; i++) {
            try {
                String url = getUrlPage(name, specie, gender, episode, status);
                String JsonResponse = HttpUtils.get(url);
                ArrayList<Personaje> list = processJson(JsonResponse);
                personaje.addAll(list);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return personaje;

    }
    private String getUrlPage(String name,String specie,String gender,String episode,String status) {

        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("name", name)
                .appendQueryParameter("species", specie)
                .appendQueryParameter("gender", gender)
                .appendQueryParameter("status", status)
                .build();
        return builtUri.toString();
    }


    private ArrayList<Personaje> processJson(String jsonResponse) {
        ArrayList<Personaje> personajes = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            /**'results' lo coge de la api, con otro nombre fallaria, contiene la informacion
             * de todos los strings del objeto.*/
            JSONArray jsonPersonajes = data.getJSONArray("results");
            for (int i = 0; i < jsonPersonajes.length(); i++) {
                JSONObject jsonPersonaje = jsonPersonajes.getJSONObject(i);


                Personaje pj=new Personaje();
                /**Aqui pasa exactamente como antes, el get.String hay que poner
                 * los nombres de cada valor por la api.
                 */
                pj.setName(jsonPersonaje.getString("name"));
                pj.setSpecies(jsonPersonaje.getString("species"));
                pj.setImageurl(jsonPersonaje.getString("image"));
                pj.setEpisode(jsonPersonaje.getString("episode"));
                pj.setStatus(jsonPersonaje.getString("status"));
                pj.setGender(jsonPersonaje.getString("gender"));
                pj.setLocation(jsonPersonaje.getString("location"));


                /**Comparamos el episodio para meterlo*/
                personajes.add(pj);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return personajes;
    }

}

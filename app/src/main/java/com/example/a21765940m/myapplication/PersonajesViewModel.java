package com.example.a21765940m.myapplication;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class PersonajesViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final PersonajeDao personajeDao;
    private LiveData<List<Personaje>> personajes;
    private MutableLiveData<List<Personaje>> personaje;
    private static final int pages=10;
    private MutableLiveData<Boolean> loading;


    public PersonajesViewModel(Application application) {
        super(application);

        this.app = application;

        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.personajeDao = appDatabase.getCartaDao();

    }

    public LiveData<List<Personaje>> getPersonajes() {
        Log.d("DEBUG", "ENTRA");

       return personajeDao.getPersonajes();
    }

    public void reload() {
        // do async operation to fetch users
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Personaje>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }

        @Override
        protected ArrayList<Personaje> doInBackground(Void... voids) {



            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );
            PersonajesAPI api = new PersonajesAPI();
            String name = preferences.getString("name", "");
            String specie = preferences.getString("specie", "");
            String gender = preferences.getString("gender", "");
            String episode= preferences.getString("episode", "");
            String status= preferences.getString("status", "");

            ArrayList<Personaje> result = api.getPersonajes(name, specie, gender, episode, status);

            if(gender.equals("todos")){
                result=api.getPersonajes(name,specie,"",episode,status);
            }
            else if(status.equals("todos")){
                result=api.getPersonajes(name,specie,gender,episode,"");
            }
            else if(gender.equals("todos") && status.equals("todos")){
                result=api.getPersonajes(name,specie,"",episode,"");
            }


            Log.d("DEBUG", result!=null?result.toString():null);
            personajeDao.deletePersonajes();
            personajeDao.addPersonajes(result);
            return result;
        }
        protected void onPostExecute(ArrayList<Personaje> personajes) {
            super.onPostExecute(personajes);
            loading.setValue(false);
        }

    }
    public MutableLiveData<Boolean> getLoading() {
        if(loading == null){
            loading = new MutableLiveData<>();
        }
        return loading;
    }

}


package com.example.a21765940m.myapplication;

import java.io.Serializable;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Personaje implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
     private String species;
     private String imageurl;
     private String episode;
     private String status;
     private String gender;
     private String location;

    public Personaje(String name, String species, String imageurl, String episode, String status, String gender, String location) {
        this.name = name;
        this.species = species;
        this.imageurl = imageurl;
        this.episode = episode;
        this.status = status;
        this.gender = gender;
        this.location = location;
    }

    public Personaje() {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Personaje{" +
                "name='" + name + '\'' +
                ", species='" + species + '\'' +
                ", imageurl='" + imageurl + '\'' +
                ", episode='" + episode + '\'' +
                ", status='" + status + '\'' +
                ", gender='" + gender + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
